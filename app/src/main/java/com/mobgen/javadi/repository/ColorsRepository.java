package com.mobgen.javadi.repository;

import java.util.List;

/**
 * @author Alejandro Platas Mallo
 * @version 1.00
 * @since 30/4/18
 */
public interface ColorsRepository {
	List<String> getColors();
}
