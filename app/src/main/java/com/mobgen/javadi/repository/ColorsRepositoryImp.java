package com.mobgen.javadi.repository;

import java.util.Arrays;
import java.util.List;

/**
 * @author Alejandro Platas Mallo
 * @version 1.00
 * @since 30/4/18
 */
public class ColorsRepositoryImp implements ColorsRepository {

	@Override
	public List<String> getColors() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return Arrays.asList("Blanco", "Rojo", "Azul", "Amarillo", "Verde", "Negro", "Violeta");
	}
}
