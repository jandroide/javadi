package com.mobgen.javadi.ui.main.view;

import com.mobgen.javadi.R;
import com.mobgen.javadi.interactors.GetColorsInteractor;
import com.mobgen.javadi.interactors.GetColorsInteractorImp;
import com.mobgen.javadi.interactors.executor.JobExecutor;
import com.mobgen.javadi.interactors.executor.PostExecutionThread;
import com.mobgen.javadi.interactors.executor.ThreadExecutor;
import com.mobgen.javadi.interactors.executor.UIThread;
import com.mobgen.javadi.repository.ColorsRepository;
import com.mobgen.javadi.repository.ColorsRepositoryImp;
import com.mobgen.javadi.ui.main.presenter.MainPresenter;
import com.mobgen.javadi.ui.main.presenter.MainPresenterImp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity implements MainView {
	private final String LOG_TAG = MainActivity.class.getSimpleName();
	private MainPresenter presenter;
	private TextView tvColors;
	private ProgressBar progressBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		initializeUi();
		initializePresenter();

		presenter.onCreate();
	}

	private void initializeUi() {
		tvColors = findViewById(R.id.tv_colors);
		progressBar = findViewById(R.id.progressBar);
	}

	private void initializePresenter() {
		ThreadExecutor threadExecutor = JobExecutor.getInstance();
		PostExecutionThread postExecutionThread = UIThread.getInstance();

		ColorsRepository repository = new ColorsRepositoryImp();
		GetColorsInteractor interactor = new GetColorsInteractorImp(repository, threadExecutor, postExecutionThread);
		presenter = new MainPresenterImp(this, interactor);
	}

	@Override
	public void showProgress() {
		progressBar.setVisibility(View.VISIBLE);
	}

	@Override
	public void hideProgress() {
		progressBar.setVisibility(View.GONE);
	}

	@Override
	public void showNoColors() {
		Log.i(LOG_TAG, "No colors");
	}

	@Override
	public void showColors(List<String> colors) {
		tvColors.setText(colors.toString());
		Log.i(LOG_TAG, colors.toString());
	}
}
