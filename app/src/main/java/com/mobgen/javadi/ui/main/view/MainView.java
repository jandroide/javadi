package com.mobgen.javadi.ui.main.view;

import java.util.List;

/**
 * @author Alejandro Platas Mallo
 * @version 1.00
 * @since 30/4/18
 */
public interface MainView {
	void showProgress();

	void hideProgress();

	void showNoColors();

	void showColors(List<String> colors);
}
