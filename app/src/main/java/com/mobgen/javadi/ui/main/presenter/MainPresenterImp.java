package com.mobgen.javadi.ui.main.presenter;

import com.mobgen.javadi.interactors.GetColorsInteractor;
import com.mobgen.javadi.ui.main.view.MainView;

import java.util.List;

/**
 * @author Alejandro Platas Mallo
 * @version 1.00
 * @since 30/4/18
 */
public class MainPresenterImp implements MainPresenter {
	private final MainView view;
	private final GetColorsInteractor interactor;

	public MainPresenterImp(MainView view, GetColorsInteractor interactor) {
		this.view = view;
		this.interactor = interactor;
	}

	@Override
	public void onCreate() {
		view.showProgress();
		interactor.execute(new GetColorsInteractor.Callback() {
			@Override
			public void onColorsLoaded(List<String> colors) {
				view.hideProgress();
				if (colors.isEmpty()) {
					view.showNoColors();
				} else {
					view.showColors(colors);
				}
			}
		});
	}
}
