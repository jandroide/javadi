package com.mobgen.javadi.ui.main.presenter;

/**
 * @author Alejandro Platas Mallo
 * @version 1.00
 * @since 30/4/18
 */
public interface MainPresenter {
	void onCreate();
}
