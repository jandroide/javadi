package com.mobgen.javadi.interactors.executor;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class JobExecutor implements ThreadExecutor {

	private static final int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();

	private static final int INITIAL_POOL_SIZE = NUMBER_OF_CORES + 1;
	private static final int MAX_POOL_SIZE = NUMBER_OF_CORES * 2 + 1;

	// Sets the amount of time an idle thread waits before terminating
	private static final int KEEP_ALIVE_TIME = 10;

	// Sets the Time Unit to seconds
	private static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;
	private final BlockingQueue<Runnable> workQueue;
	private final ThreadPoolExecutor threadPoolExecutor;

	private JobExecutor() {
		workQueue = new LinkedBlockingQueue<>();
		threadPoolExecutor = new ThreadPoolExecutor(INITIAL_POOL_SIZE, MAX_POOL_SIZE, KEEP_ALIVE_TIME, KEEP_ALIVE_TIME_UNIT, this.workQueue);
	}

	public static JobExecutor getInstance() {
		return LazyHolder.INSTANCE;
	}

	@Override
	public void execute(Runnable runnable) {
		if (runnable == null) {
			throw new IllegalArgumentException("Runnable to execute cannot be null");
		}

		this.threadPoolExecutor.execute(runnable);
	}

	private static class LazyHolder {
		private static final JobExecutor INSTANCE = new JobExecutor();
	}
}