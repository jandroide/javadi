package com.mobgen.javadi.interactors.executor;

/**
 * Created: Alejandro Platas
 * Date: 4/4/17.
 */

public interface ThreadExecutor {
	void execute(final Runnable runnable);
}
