package com.mobgen.javadi.interactors;

import java.util.List;

/**
 * @author Alejandro Platas Mallo
 * @version 1.00
 * @since 30/4/18
 */
public interface GetColorsInteractor {
	void execute(Callback callback);

	interface Callback {
		void onColorsLoaded(List<String> colors);
		}
}
