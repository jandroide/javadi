package com.mobgen.javadi.interactors;

import com.mobgen.javadi.interactors.executor.PostExecutionThread;
import com.mobgen.javadi.interactors.executor.ThreadExecutor;
import com.mobgen.javadi.repository.ColorsRepository;

import java.util.List;

/**
 * @author Alejandro Platas Mallo
 * @version 1.00
 * @since 30/4/18
 */
public class GetColorsInteractorImp implements GetColorsInteractor, Runnable {

	private ColorsRepository repository;
	private ThreadExecutor threadExecutor;
	private PostExecutionThread postExecutionThread;
	private Callback callback;

	public GetColorsInteractorImp(ColorsRepository repository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
		this.repository = repository;
		this.threadExecutor = threadExecutor;
		this.postExecutionThread = postExecutionThread;
	}

	@Override
	public void execute(Callback callback) {
		if(callback == null) {
			throw new IllegalArgumentException("Interactor callback cannot be null");
		}

		this.callback = callback;
		threadExecutor.execute(this);
	}

	@Override
	public void run() {
		final List<String> colors = repository.getColors();
		postExecutionThread.post(new Runnable() {
			@Override
			public void run() {
				callback.onColorsLoaded(colors);
			}
		});
	}
}
